package ir.payment.grpc.generalmessage;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.11.0)",
    comments = "Source: GeneralMessage.proto")
public final class GeneralMessageServiceGrpc {

  private GeneralMessageServiceGrpc() {}

  public static final String SERVICE_NAME = "ir.payment.grpc.generalmessage.GeneralMessageService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getPepIsoTerminalSwitchMethod()} instead. 
  public static final io.grpc.MethodDescriptor<ir.payment.grpc.generalmessage.GeneralMessageReq,
      ir.payment.grpc.generalmessage.GeneralMessageRes> METHOD_PEP_ISO_TERMINAL_SWITCH = getPepIsoTerminalSwitchMethodHelper();

  private static volatile io.grpc.MethodDescriptor<ir.payment.grpc.generalmessage.GeneralMessageReq,
      ir.payment.grpc.generalmessage.GeneralMessageRes> getPepIsoTerminalSwitchMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<ir.payment.grpc.generalmessage.GeneralMessageReq,
      ir.payment.grpc.generalmessage.GeneralMessageRes> getPepIsoTerminalSwitchMethod() {
    return getPepIsoTerminalSwitchMethodHelper();
  }

  private static io.grpc.MethodDescriptor<ir.payment.grpc.generalmessage.GeneralMessageReq,
      ir.payment.grpc.generalmessage.GeneralMessageRes> getPepIsoTerminalSwitchMethodHelper() {
    io.grpc.MethodDescriptor<ir.payment.grpc.generalmessage.GeneralMessageReq, ir.payment.grpc.generalmessage.GeneralMessageRes> getPepIsoTerminalSwitchMethod;
    if ((getPepIsoTerminalSwitchMethod = GeneralMessageServiceGrpc.getPepIsoTerminalSwitchMethod) == null) {
      synchronized (GeneralMessageServiceGrpc.class) {
        if ((getPepIsoTerminalSwitchMethod = GeneralMessageServiceGrpc.getPepIsoTerminalSwitchMethod) == null) {
          GeneralMessageServiceGrpc.getPepIsoTerminalSwitchMethod = getPepIsoTerminalSwitchMethod = 
              io.grpc.MethodDescriptor.<ir.payment.grpc.generalmessage.GeneralMessageReq, ir.payment.grpc.generalmessage.GeneralMessageRes>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "ir.payment.grpc.generalmessage.GeneralMessageService", "pepIsoTerminalSwitch"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ir.payment.grpc.generalmessage.GeneralMessageReq.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ir.payment.grpc.generalmessage.GeneralMessageRes.getDefaultInstance()))
                  .setSchemaDescriptor(new GeneralMessageServiceMethodDescriptorSupplier("pepIsoTerminalSwitch"))
                  .build();
          }
        }
     }
     return getPepIsoTerminalSwitchMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static GeneralMessageServiceStub newStub(io.grpc.Channel channel) {
    return new GeneralMessageServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static GeneralMessageServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new GeneralMessageServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static GeneralMessageServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new GeneralMessageServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class GeneralMessageServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void pepIsoTerminalSwitch(ir.payment.grpc.generalmessage.GeneralMessageReq request,
        io.grpc.stub.StreamObserver<ir.payment.grpc.generalmessage.GeneralMessageRes> responseObserver) {
      asyncUnimplementedUnaryCall(getPepIsoTerminalSwitchMethodHelper(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getPepIsoTerminalSwitchMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                ir.payment.grpc.generalmessage.GeneralMessageReq,
                ir.payment.grpc.generalmessage.GeneralMessageRes>(
                  this, METHODID_PEP_ISO_TERMINAL_SWITCH)))
          .build();
    }
  }

  /**
   */
  public static final class GeneralMessageServiceStub extends io.grpc.stub.AbstractStub<GeneralMessageServiceStub> {
    private GeneralMessageServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GeneralMessageServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GeneralMessageServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GeneralMessageServiceStub(channel, callOptions);
    }

    /**
     */
    public void pepIsoTerminalSwitch(ir.payment.grpc.generalmessage.GeneralMessageReq request,
        io.grpc.stub.StreamObserver<ir.payment.grpc.generalmessage.GeneralMessageRes> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getPepIsoTerminalSwitchMethodHelper(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class GeneralMessageServiceBlockingStub extends io.grpc.stub.AbstractStub<GeneralMessageServiceBlockingStub> {
    private GeneralMessageServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GeneralMessageServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GeneralMessageServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GeneralMessageServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ir.payment.grpc.generalmessage.GeneralMessageRes pepIsoTerminalSwitch(ir.payment.grpc.generalmessage.GeneralMessageReq request) {
      return blockingUnaryCall(
          getChannel(), getPepIsoTerminalSwitchMethodHelper(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class GeneralMessageServiceFutureStub extends io.grpc.stub.AbstractStub<GeneralMessageServiceFutureStub> {
    private GeneralMessageServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private GeneralMessageServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GeneralMessageServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new GeneralMessageServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ir.payment.grpc.generalmessage.GeneralMessageRes> pepIsoTerminalSwitch(
        ir.payment.grpc.generalmessage.GeneralMessageReq request) {
      return futureUnaryCall(
          getChannel().newCall(getPepIsoTerminalSwitchMethodHelper(), getCallOptions()), request);
    }
  }

  private static final int METHODID_PEP_ISO_TERMINAL_SWITCH = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final GeneralMessageServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(GeneralMessageServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_PEP_ISO_TERMINAL_SWITCH:
          serviceImpl.pepIsoTerminalSwitch((ir.payment.grpc.generalmessage.GeneralMessageReq) request,
              (io.grpc.stub.StreamObserver<ir.payment.grpc.generalmessage.GeneralMessageRes>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class GeneralMessageServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    GeneralMessageServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ir.payment.grpc.generalmessage.GeneralMessage.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("GeneralMessageService");
    }
  }

  private static final class GeneralMessageServiceFileDescriptorSupplier
      extends GeneralMessageServiceBaseDescriptorSupplier {
    GeneralMessageServiceFileDescriptorSupplier() {}
  }

  private static final class GeneralMessageServiceMethodDescriptorSupplier
      extends GeneralMessageServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    GeneralMessageServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (GeneralMessageServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new GeneralMessageServiceFileDescriptorSupplier())
              .addMethod(getPepIsoTerminalSwitchMethodHelper())
              .build();
        }
      }
    }
    return result;
  }
}
