package ir.payment.gateway.config;

import ir.payment.gateway.tcp.integration.GWDeserializer;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.packager.GenericPackager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Transformers;
import org.springframework.integration.ip.dsl.Tcp;
import org.springframework.integration.ip.tcp.serializer.TcpCodecs;

@EnableIntegration
@Configuration
public class IntegrationConfig {

    private final TcpServerProps props;

    private final MessageTransformer messageTransformer;

    public IntegrationConfig(TcpServerProps props, MessageTransformer messageTransformer) {
        this.props = props;
        this.messageTransformer = messageTransformer;
    }



    @Bean
    public IntegrationFlow integrationFlow() {
        return IntegrationFlows.from(Tcp.inboundGateway(Tcp.nioServer(this.props.getPort())
                .deserializer(new GWDeserializer())
                .get()))
                .transform(this.messageTransformer)
                .get();
    }



}
