package ir.payment.gateway.config;

//import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;
import ir.payment.gateway.external.service.MessageDTO;
import ir.payment.gateway.external.service.SampleMicroService;
import ir.payment.gateway.service.GeneralMessageClient;
import org.bouncycastle.util.encoders.Hex;
import org.jpos.iso.ISOPackager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.transformer.GenericTransformer;
import org.springframework.stereotype.Component;
import java.io.IOException;

@Component
public class MessageTransformer implements GenericTransformer<byte[], String> {

    private final GeneralMessageClient generalMessageClient;
    private final SampleMicroService sampleMicroService;

    @Autowired
    public MessageTransformer(GeneralMessageClient generalMessageClient, SampleMicroService sampleMicroService) {
        this.generalMessageClient = generalMessageClient;
        this.sampleMicroService = sampleMicroService;
    }


    @Override
    public String transform(byte[] payload) {
//        MessageDTO messageDTO = new MessageDTO(HexBin.encode(payload));
        MessageDTO messageDTO = new MessageDTO(Hex.toHexString(payload));
        try {
            this.sampleMicroService.processPosSentMessage(messageDTO);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        return generalMessageClient.pepISOTerminalSwitchJustPayload(HexBin.encode(payload));
        return generalMessageClient.pepISOTerminalSwitchJustPayload(Hex.toHexString(payload));


        /*ISOMsg isoMsg = new ISOMsg();
        isoMsg.setPackager(packager);
        synchronized (this) {
            try {
                packager.unpack(isoMsg, payload);
            } catch (ISOException e) {
                e.printStackTrace();
            }
        }
        return generalMessageClient.pepISOTerminalSwitch(ByteString.copyFrom(payload), isoMsg.getString(2), isoMsg.getString(3));
*/
    }

}
