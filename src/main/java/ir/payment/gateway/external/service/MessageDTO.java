package ir.payment.gateway.external.service;

import com.google.protobuf.ByteString;

/**
 * @CreatedBy Hadi Rasouli
 */

public class MessageDTO {

    private String message;

    public MessageDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
