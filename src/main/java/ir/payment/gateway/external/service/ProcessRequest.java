package ir.payment.gateway.external.service;

import java.io.Serializable;

public class ProcessRequest implements Serializable {
    private String message;

    ProcessRequest(String message) {
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    @Override
    public String toString() {
        return "{" +
                "\"message\"=\"" + getMessage() + "\""+
                "}";
    }
}
