package ir.payment.gateway.external.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ir.payment.gateway.config.KafkaProperties;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @CreatedBy Hadi Rasouli
 */

@Service
public class ProcessKafkaService {

    private final Logger log = LoggerFactory.getLogger(ProcessKafkaService.class);

    private static final String TOPIC = "topic_alert";

    private final KafkaProperties kafkaProperties;

    private final static Logger logger = LoggerFactory.getLogger(ProcessKafkaService.class);
    private KafkaProducer<String, String> producer;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public ProcessKafkaService(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }

    @PostConstruct
    public void initialize(){
        log.info("Kafka producer initializing...");
        this.producer = new KafkaProducer<>(kafkaProperties.getProducerProps());
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
        log.info("Kafka producer initialized");
    }

    public void informProcess(MessageDTO terminalMessage) {
        try {
            String message = objectMapper.writeValueAsString(terminalMessage);
            ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, message);
            producer.send(record);
            logger.error("Kafka Message sent {}", record);
        } catch (JsonProcessingException e) {
            logger.error("Could not send store alert", e);
        }
    }

    @PreDestroy
    public void shutdown() {
        log.info("Shutdown Kafka producer");
        producer.close();
    }
}
