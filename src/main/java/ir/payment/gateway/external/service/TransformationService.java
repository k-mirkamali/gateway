package ir.payment.gateway.external.service;

import ir.payment.gateway.external.utils.ServiceGatewayConfig;
import ir.payment.gateway.service.GeneralMessageClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

/**
 * @CreatedBy K.Mirkamali
 * ON 9/21/21
 **/
@Service
public class TransformationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralMessageClient.class);

    @Value("${gateway.url}")
    private String gatewayUrl;

    public final ServiceGatewayConfig serviceGatewayConfig;
    public final ProcessKafkaService processKafkaService;

    public TransformationService(ServiceGatewayConfig serviceGatewayConfig, ProcessKafkaService processKafkaService) {
        this.serviceGatewayConfig = serviceGatewayConfig;
        this.processKafkaService = processKafkaService;
    }

    public void processPosSentMessage(MessageDTO message) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        // Get
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(this.gatewayUrl + "/transformatoinmicro/api/sample/public/auth");
        HttpEntity<?> entityGet = new HttpEntity<>(headers);
        HttpEntity<String> responseGet = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entityGet,
                String.class);
        LOGGER.info("gatewayResponse received GET {}", responseGet.getBody());

        // Post
        builder = UriComponentsBuilder.fromHttpUrl(this.gatewayUrl + "/samplemicro1/api/sample/public/process");
        HttpEntity<ProcessRequest> entityPost = new HttpEntity<>(new ProcessRequest(message.getMessage()),headers);
        HttpEntity<String> responsePost = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                entityPost,
                String.class);
        LOGGER.info("gatewayResponse received POST {}", responsePost.getBody());

        // Kafka Sample
        processKafkaService.informProcess(message);
    }
}
