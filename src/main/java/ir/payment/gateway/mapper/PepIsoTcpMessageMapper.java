package ir.payment.gateway.mapper;

import ir.payment.gateway.tcp.integration.GWTcpNioConnection;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.packager.GenericPackager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.ip.tcp.connection.TcpConnection;
import org.springframework.integration.ip.tcp.connection.TcpNioConnection;
import org.springframework.integration.mapping.BytesMessageMapper;
import org.springframework.lang.Nullable;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @CreatedBy K.Mirkamali
 * ON 9/21/21
 **/
@Component
public class PepIsoTcpMessageMapper extends GeneralTcpMessageMapper{
    private BytesMessageMapper bytesMessageMapper;


    @Override
    public Message toMessage(TcpConnection connection, @Nullable Map<String, Object> headers) {
//        GeneralMessage message = new GeneralMessage();
        byte[] payload = null;
        ISOMsg isoMsg = new ISOMsg();
//        ISOPackager isoPackager = this.getPackager();
        if (connection instanceof TcpNioConnection){
            GWTcpNioConnection tcpNioConnection = (GWTcpNioConnection) connection;
            payload = (byte[])tcpNioConnection.getPayload();
        }
        if (payload != null) {
            /************** Sina! Here is where you are going to change ******/
            try {
                isoMsg = new ISOMsg();
//                isoMsg.setPackager(isoPackager);
                synchronized (this) {
//                    packager.unpack(isoMsg, payload);
                }
                logger.info("isoMsg: " + isoMsg.toString());
//                message.setIsoMsg(isoMsg);
            } catch (Exception e) {
                e.printStackTrace();
            }
            /**************** This part is commented permanently ****************/
            /*AbstractIntegrationMessageBuilder messageBuilder;
            if (this.bytesMessageMapper != null && payload instanceof byte[]) {
                messageBuilder = this.getMessageBuilderFactory().fromMessage(this.bytesMessageMapper.toMessage((byte[])((byte[])payload)));
            } else {
                messageBuilder = this.getMessageBuilderFactory().withPayload(payload);
            }
            MessageHeaders messageHeaders = new MutableMessageHeaders((Map)null);
            this.addStandardHeaders(connection, messageHeaders);
            this.addCustomHeaders(connection, messageHeaders);
            message = messageBuilder.copyHeaders(messageHeaders).copyHeadersIfAbsent(headers).build();*/
        } else if (this.logger.isWarnEnabled()) {
            this.logger.warn("Null payload from connection " + connection.getConnectionId());
        }

        return null;
    }

    @Override
    public Object fromMessage(Message message) {
        if (this.bytesMessageMapper != null) {
            return this.bytesMessageMapper.fromMessage(message);
        } else {
            return /*this.stringToBytes ? this.getPayloadAsBytes(message) :*/ message.getPayload();
        }
    }

}
