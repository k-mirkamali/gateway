package ir.payment.gateway.mapper;

import org.jpos.iso.ISOMsg;
import org.springframework.integration.ip.tcp.connection.TcpConnection;
import org.springframework.integration.ip.tcp.connection.TcpMessageMapper;
import org.springframework.integration.mapping.BytesMessageMapper;
import org.springframework.lang.Nullable;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @CreatedBy K.Mirkamali
 * ON 9/21/21
 **/
@Component
public abstract class GeneralTcpMessageMapper extends TcpMessageMapper {
    private BytesMessageMapper bytesMessageMapper;

    public BytesMessageMapper getBytesMessageMapper() {
        return bytesMessageMapper;
    }

    @Override
    public void setBytesMessageMapper(BytesMessageMapper bytesMessageMapper) {
        this.bytesMessageMapper = bytesMessageMapper;
    }

    @Override
    public abstract Message<ISOMsg> toMessage(TcpConnection connection, @Nullable Map<String, Object> headers);

    @Override
    public abstract Object fromMessage(Message<?> message);

}
