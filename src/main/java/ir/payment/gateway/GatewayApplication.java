package ir.payment.gateway;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.packager.GenericPackager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.config.EnableIntegration;


@SpringBootApplication
@EnableIntegration
public class GatewayApplication {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(GatewayApplication.class);
        SpringApplication.run(GatewayApplication.class,args);
    }

//    @Bean
//    public ISOPackager packager() throws ISOException {
//        return new GenericPackager("src/main/resources/protocols/pepiso.xml");
//    }
}
