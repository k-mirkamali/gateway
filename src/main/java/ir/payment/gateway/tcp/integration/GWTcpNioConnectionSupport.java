package ir.payment.gateway.tcp.integration;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.integration.ip.tcp.connection.AbstractTcpConnectionSupport;
import org.springframework.integration.ip.tcp.connection.DefaultTcpNioConnectionSupport;
import org.springframework.integration.ip.tcp.connection.TcpNioConnection;
import org.springframework.integration.ip.tcp.connection.TcpNioConnectionSupport;
import org.springframework.lang.Nullable;

import java.io.InputStream;
import java.io.PushbackInputStream;
import java.nio.channels.SocketChannel;

public class GWTcpNioConnectionSupport extends AbstractTcpConnectionSupport implements TcpNioConnectionSupport {
    @Override
    public TcpNioConnection createNewConnection(SocketChannel socketChannel, boolean server, boolean lookupHost, @Nullable ApplicationEventPublisher applicationEventPublisher, String connectionFactoryName) {
        return (GWTcpNioConnection)(this.isPushbackCapable() ?
                new GWTcpNioConnectionSupport.PushBackTcpNioConnection(socketChannel, server,
                        lookupHost, applicationEventPublisher, connectionFactoryName,this.getPushbackBufferSize()) :
                new GWTcpNioConnection(socketChannel, server, lookupHost, applicationEventPublisher, connectionFactoryName));
    }

    private static final class PushBackTcpNioConnection extends TcpNioConnection {
        private final int pushbackBufferSize;
        private final String connectionId;
        private volatile PushbackInputStream pushbackStream;
        private volatile InputStream wrapped;

        PushBackTcpNioConnection(SocketChannel socketChannel, boolean server, boolean lookupHost, @Nullable ApplicationEventPublisher applicationEventPublisher, @Nullable String connectionFactoryName, int bufferSize) {
            super(socketChannel, server, lookupHost, applicationEventPublisher, connectionFactoryName);
            this.pushbackBufferSize = bufferSize;
            this.connectionId = "pushback:" + super.getConnectionId();
        }

        protected InputStream inputStream() {
            InputStream wrappedStream = super.inputStream();
            if (this.pushbackStream == null || !wrappedStream.equals(this.wrapped)) {
                this.pushbackStream = new PushbackInputStream(wrappedStream, this.pushbackBufferSize);
                this.wrapped = wrappedStream;
            }

            return this.pushbackStream;
        }

        public String getConnectionId() {
            return this.connectionId;
        }
    }

}
