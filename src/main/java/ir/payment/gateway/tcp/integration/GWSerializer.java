package ir.payment.gateway.tcp.integration;

import org.springframework.core.serializer.Serializer;

import java.io.IOException;
import java.io.OutputStream;

public class GWSerializer implements Serializer<byte[]> {
    @Override
    public void serialize(byte[] bytes, OutputStream outputStream) throws IOException {

    }

    @Override
    public byte[] serializeToByteArray(byte[] object) throws IOException {
        return new byte[0];
    }
}
