package ir.payment.gateway.tcp.integration;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.integration.ip.tcp.connection.NoListenerException;
import org.springframework.integration.ip.tcp.connection.TcpListener;
import org.springframework.integration.ip.tcp.connection.TcpMessageMapper;
import org.springframework.integration.ip.tcp.connection.TcpNioConnection;
import org.springframework.integration.ip.tcp.serializer.SoftEndOfStreamException;
import org.springframework.integration.util.CompositeExecutor;
import org.springframework.lang.Nullable;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

import javax.net.ssl.SSLSession;
import java.io.*;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class GWTcpNioConnection extends TcpNioConnection {
    private static final byte[] EOF = new byte[0];
//    private GeneralTcpMessageMapper mapper;
    private final AtomicInteger executionControl = new AtomicInteger();
    private volatile CompositeExecutor taskExecutor;
    private volatile CountDownLatch writingLatch;
    private volatile int maxMessageSize = 61440;
    private volatile boolean writingToPipe;
    private volatile ByteBuffer rawBuffer;
    private final SocketChannel socketChannel;
    private boolean usingDirectBuffers;
    private long pipeTimeout = 60000L;
    private volatile OutputStream bufferedOutputStream;
    private volatile boolean timedOut;
    private volatile long lastRead;
    private volatile long lastSend;

    public GWTcpNioConnection(SocketChannel socketChannel, boolean server, boolean lookupHost, ApplicationEventPublisher applicationEventPublisher, String connectionFactoryName) {
        super(socketChannel, server, lookupHost, applicationEventPublisher, connectionFactoryName);
        this.socketChannel = socketChannel;
    }

    @Override
    public void setPipeTimeout(long pipeTimeout) {
        this.pipeTimeout = pipeTimeout;
    }

    @Override
    public void close() {
        this.setNoReadErrorOnClose(true);
        this.doClose();
    }

    private void doClose() {
        try {
            super.inputStream().close();
        } catch (IOException var3) {
        }

        try {
            this.socketChannel.close();
        } catch (Exception var2) {
        }

        super.close();
    }

    @Override
    public boolean isOpen() {
        return this.socketChannel.isOpen();
    }

    /*@Override
    public void send(Message message) {
        synchronized(this.socketChannel) {
            try {
                if (this.bufferedOutputStream == null) {
                    int writeBufferSize = this.socketChannel.socket().getSendBufferSize();
                    this.bufferedOutputStream = new BufferedOutputStream(this.getChannelOutputStream(), writeBufferSize > 0 ? writeBufferSize : 8192);
                }

                Object object = this.getMapper().fromMessage(message);
                Assert.state(object != null, "Mapper mapped the message to 'null'.");
                this.lastSend = System.currentTimeMillis();
                this.getSerializer().serialize(object, this.bufferedOutputStream);
                this.bufferedOutputStream.flush();
            } catch (Exception var6) {
                MessagingException mex = new MessagingException(message, "Send Failed", var6);
                this.publishConnectionExceptionEvent(mex);
                this.closeConnection(true);
                throw mex;
            }

            if (this.logger.isDebugEnabled()) {
                this.logger.debug(this.getConnectionId() + " Message sent " + message);
            }

        }
    }*/

    @Override
    public Object getPayload() {
        InputStream inputStream;
        try {
            inputStream = this.inputStream();
            return this.getDeserializer().deserialize(inputStream);
        } catch (IOException var3) {
            throw new UncheckedIOException(var3);
        }
    }

    @Override
    public int getPort() {
        return this.socketChannel.socket().getPort();
    }

    @Override
    public Object getDeserializerStateKey() {
        return this.inputStream();
    }

    @Nullable
    @Override
    public SSLSession getSslSession() {
        return null;
    }

    @Override
    protected ByteBuffer allocate(int length) {
        ByteBuffer buffer;
        if (this.usingDirectBuffers) {
            buffer = ByteBuffer.allocateDirect(length);
        } else {
            buffer = ByteBuffer.allocate(length);
        }

        return buffer;
    }

    @Override
    public void run() {
        if (this.logger.isTraceEnabled()) {
            this.logger.trace(this.getConnectionId() + " Nio message assembler running...");
        }

        boolean moreDataAvailable = true;

        while(moreDataAvailable) {
            boolean var15 = false;

            label281: {
                try {
                    try {
                        var15 = true;
                        if (!dataAvailable()) {
                            this.executionControl.decrementAndGet();
                            var15 = false;
                        } else {
                            Message<?> message = this.convert();
                            if (this.dataAvailable()) {
                                this.executionControl.incrementAndGet();

                                try {
                                    this.taskExecutor.execute2(this);
                                } catch (RejectedExecutionException var19) {
                                    this.executionControl.decrementAndGet();
                                    if (this.logger.isInfoEnabled()) {
                                        this.logger.info(this.getConnectionId() + " Insufficient threads in the assembler fixed thread pool; consider increasing this task executor pool size; data avail: "
                                                + super.inputStream().available());
                                    }
                                }
                            }

                            this.executionControl.decrementAndGet();
                            if (message != null) {
                                this.sendToChannel(message);
                                var15 = false;
                            } else {
                                var15 = false;
                            }
                        }
                        break label281;
                    } catch (Exception var20) {
                        if (this.logger.isTraceEnabled()) {
                            this.logger.error("Read exception " + this.getConnectionId(), var20);
                        } else if (!this.isNoReadErrorOnClose()) {
                            this.logger.error("Read exception " + this.getConnectionId() + " " + var20.getClass().getSimpleName() + ":" + var20.getCause() + ":" + var20.getMessage());
                        } else if (this.logger.isDebugEnabled()) {
                            this.logger.debug("Read exception " + this.getConnectionId() + " " + var20.getClass().getSimpleName() + ":" + var20.getCause() + ":" + var20.getMessage());
                        }
                        this.sendExceptionToListener(var20);
                    }

                    this.closeConnection(true);

                    var15 = false;
                } finally {
                    if (var15) {
                        moreDataAvailable = false;
                        try {
                            if (this.dataAvailable()) {
                                synchronized(this.executionControl) {
                                    if (this.executionControl.incrementAndGet() <= 1) {
                                        this.executionControl.set(1);
                                        moreDataAvailable = true;
                                    } else {
                                        this.executionControl.decrementAndGet();
                                    }
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (moreDataAvailable) {
                            if (this.logger.isTraceEnabled()) {
                                this.logger.trace(this.getConnectionId() + " Nio message assembler continuing...");
                            }
                        } else if (this.logger.isTraceEnabled()) {
                            try {
                                this.logger.trace(this.getConnectionId() + " Nio message assembler exiting... avail: "
                                        + super.inputStream().available());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }

                moreDataAvailable = false;
                try {
                    if (this.dataAvailable()) {
                        synchronized(this.executionControl) {
                            if (this.executionControl.incrementAndGet() <= 1) {
                                this.executionControl.set(1);
                                moreDataAvailable = true;
                            } else {
                                this.executionControl.decrementAndGet();
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (moreDataAvailable) {
                    if (this.logger.isTraceEnabled()) {
                        this.logger.trace(this.getConnectionId() + " Nio message assembler continuing...");
                    }
                } else if (this.logger.isTraceEnabled()) {
                    try {
                        this.logger.trace(this.getConnectionId() + " Nio message assembler exiting... avail: "
                                + super.inputStream().available());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                return;
            }

            moreDataAvailable = false;
            try {
                if (this.dataAvailable()) {
                    synchronized(this.executionControl) {
                        if (this.executionControl.incrementAndGet() <= 1) {
                            this.executionControl.set(1);
                            moreDataAvailable = true;
                        } else {
                            this.executionControl.decrementAndGet();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (moreDataAvailable) {
                if (this.logger.isTraceEnabled()) {
                    this.logger.trace(this.getConnectionId() + " Nio message assembler continuing...");
                }
            } else if (this.logger.isTraceEnabled()) {
                try {
                    this.logger.trace(this.getConnectionId() + " Nio message assembler exiting... avail: "
                            + super.inputStream().available());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean dataAvailable() throws IOException {
        if (this.logger.isTraceEnabled()) {
            this.logger.trace(this.getConnectionId() + " checking data avail: " + super.inputStream().available() + " pending: " + this.writingToPipe);
        }

        return this.writingToPipe || super.inputStream().available() > 0;
    }

    private synchronized Message<?> convert() throws IOException {
        if (this.logger.isTraceEnabled()) {
            this.logger.trace(this.getConnectionId() + " checking data avail (convert): "
                    + super.inputStream().available() + " pending: " + this.writingToPipe);
        }

        if (super.inputStream().available() <= 0) {
            try {
                if (!this.writingLatch.await(60L, TimeUnit.SECONDS)) {
                    throw new IOException("Timed out waiting for IO");
                }

                if (super.inputStream().available() <= 0) {
                    return null;
                }
            } catch (InterruptedException var3) {
                Thread.currentThread().interrupt();
                throw new IOException("Interrupted waiting for IO", var3);
            }
        }

        Message message = null;

        try {
            message = this.getMapper().toMessage(this);
            return message;
        } catch (Exception var4) {
            this.closeConnection(true);
            if (var4 instanceof SocketTimeoutException) {
                if (this.logger.isDebugEnabled()) {
                    this.logger.debug("Closing socket after timeout " + this.getConnectionId());
                }
            } else if (!(var4 instanceof SoftEndOfStreamException)) {
                throw var4;
            }

            return null;
        }
    }

    private void sendToChannel(Message<?> message) {
        try {
            TcpListener listener = this.getListener();
            if (listener == null) {
                throw new NoListenerException("No listener");
            }

            listener.onMessage(message);
        } catch (Exception var3) {
            if (var3 instanceof NoListenerException) {
                if (this.logger.isWarnEnabled()) {
                    this.logger.warn("Unexpected message - no endpoint registered with connection: " + this.getConnectionId() + " - " + message);
                }
            } else {
                this.logger.error("Exception sending message: " + message, var3);
            }
        }
    }

    private void doRead() throws IOException {
        if (this.rawBuffer == null) {
            this.rawBuffer = this.allocate(this.maxMessageSize);
        }

        this.writingLatch = new CountDownLatch(1);
        this.writingToPipe = true;

        try {
            if (this.taskExecutor == null) {
                ExecutorService executor = Executors.newCachedThreadPool();
                this.taskExecutor = new CompositeExecutor(executor, executor);
            }

            this.checkForAssembler();
            if (this.logger.isTraceEnabled()) {
                this.logger.trace("Before read: " + this.rawBuffer.position() + "/" + this.rawBuffer.limit());
            }

            int len = this.socketChannel.read(this.rawBuffer);
            if (len < 0) {
                this.writingToPipe = false;
                this.closeConnection(true);
            }

            if (this.logger.isTraceEnabled()) {
                this.logger.trace("After read: " + this.rawBuffer.position() + "/" + this.rawBuffer.limit());
            }

            this.rawBuffer.flip();
            if (this.logger.isTraceEnabled()) {
                this.logger.trace("After flip: " + this.rawBuffer.position() + "/" + this.rawBuffer.limit());
            }

            if (this.logger.isDebugEnabled()) {
                this.logger.debug("Read " + this.rawBuffer.limit() + " into raw buffer");
            }

            this.sendToPipe(this.rawBuffer);
        } catch (IOException var5) {
            this.publishConnectionExceptionEvent(var5);
            throw var5;
        } finally {
            this.writingToPipe = false;
            this.writingLatch.countDown();
        }

    }

    /*@Override
    protected void sendToPipe(ByteBuffer rawBufferToSend) throws IOException {
        Assert.notNull(rawBufferToSend, "rawBuffer cannot be null");
        if (this.logger.isTraceEnabled()) {
            this.logger.trace(this.getConnectionId() + " Sending " + rawBufferToSend.limit() + " to pipe");
        }

        super.inputStream().write(rawBufferToSend);
        rawBufferToSend.clear();
    }*/

    private void checkForAssembler() {
        synchronized(this.executionControl) {
            if (this.executionControl.incrementAndGet() <= 1) {
                this.executionControl.set(1);
                if (this.logger.isDebugEnabled()) {
                    this.logger.debug(this.getConnectionId() + " Running an assembler");
                }

                try {
                    this.taskExecutor.execute2(this);
                } catch (RejectedExecutionException var4) {
                    this.executionControl.decrementAndGet();
                    if (this.logger.isInfoEnabled()) {
                        this.logger.info("Insufficient threads in the assembler fixed thread pool; consider increasing this task executor pool size");
                    }

                    throw var4;
                }
            } else {
                this.executionControl.decrementAndGet();
            }

        }
    }

    @Override
    public void readPacket() {
        if (this.logger.isDebugEnabled()) {
            this.logger.debug(this.getConnectionId() + " Reading...");
        }

        try {
            this.doRead();
        } catch (ClosedChannelException var2) {
            if (this.logger.isDebugEnabled()) {
                this.logger.debug(this.getConnectionId() + " Channel is closed");
            }

            this.closeConnection(true);
        } catch (Exception var3) {
            this.logger.error("Exception on Read " + this.getConnectionId() + " " + var3.getMessage(), var3);
            this.closeConnection(true);
        }

    }


    void timeout() {
        this.timedOut = true;
        this.closeConnection(true);
    }

    @Override
    public void setTaskExecutor(Executor taskExecutor) {
        if (taskExecutor instanceof CompositeExecutor) {
            this.taskExecutor = (CompositeExecutor)taskExecutor;
        } else {
            this.taskExecutor = new CompositeExecutor(taskExecutor, taskExecutor);
        }

    }
    @Override
    public void setUsingDirectBuffers(boolean usingDirectBuffers) {
        this.usingDirectBuffers = usingDirectBuffers;
    }

    @Override
    protected boolean isUsingDirectBuffers() {
        return this.usingDirectBuffers;
    }



    @Override
    public long getLastRead() {
        return this.lastRead;
    }

    @Override
    public void setLastRead(long lastRead) {
        this.lastRead = lastRead;
    }

    @Override
    public long getLastSend() {
        return this.lastSend;
    }

    @Override
    public void shutdownInput() throws IOException {
        this.socketChannel.shutdownInput();
    }

    @Override
    public void shutdownOutput() throws IOException {
        this.socketChannel.shutdownOutput();
    }


//    @Override
//    public GeneralTcpMessageMapper getMapper() {
//        return this.mapper;
//    }
//
//    @Override
//    public void setMapper(TcpMessageMapper mapper) {
//        Assert.notNull(mapper, this.getClass().getName() + " Mapper may not be null");
//        this.mapper = (GeneralTcpMessageMapper) mapper;
/*
        if (this.serializer != null && !(this.serializer instanceof AbstractByteArraySerializer)) {
            mapper.setStringToBytes(false);
        }
*/

//    }




}
