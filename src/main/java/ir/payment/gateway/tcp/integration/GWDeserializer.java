package ir.payment.gateway.tcp.integration;

//import com.sun.org.apache.xerces.internal.impl.dv.util.HexBin;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.core.serializer.Deserializer;
import org.springframework.integration.ip.tcp.serializer.SoftEndOfStreamException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class GWDeserializer implements Deserializer<byte[]> {

    private int maxMessageSize = 4096;

    @Override
    public byte[] deserialize(InputStream inputStream) throws IOException {
        int n = 0;
        int bite = 0;
        byte[] buffer;
        byte[] header = new byte[2];
        try {
            while(n < header.length) {
                bite=inputStream.read();
                if (bite < 0 && n == 0) {
                    throw new SoftEndOfStreamException("Stream closed between payloads");
                }
                header[n++] = (byte)bite;
            }
            n=0;
//            Long length = Long.parseLong(HexBin.encode(header), 16);
            Long length = Long.parseLong(Hex.toHexString(header), 16);
            buffer = new byte[length.intValue()];
            while (n < length) {
                bite=inputStream.read();
                if (bite < 0 && n == 0) {
                    throw new SoftEndOfStreamException("Stream closed between payloads");
                }
                buffer[n++] = (byte)bite;
            }
//            System.out.println("Binary is read from input: " + HexBin.encode(buffer));
            System.out.println("Binary is read from input: " + Hex.toHexString(buffer) );
//            throw new IOException("CRLF not found before max message length: " + this.maxMessageSize);
        } catch (SoftEndOfStreamException var6) {
            throw var6;
        } catch (IOException var7) {
//            this.publishEvent(var7, buffer, n);
            throw var7;
        } catch (RuntimeException var8) {
//            this.publishEvent(var8, buffer, n);
            throw var8;
        }
        return buffer;
    }

    protected void checkClosure(int bite) throws IOException {
        if (bite < 0) {
//            this.logger.debug("Socket closed during message assembly");
            throw new IOException("Socket closed during message assembly");
        }
    }

    @Override
    public byte[] deserializeFromByteArray(byte[] serialized) throws IOException {
        return serialized;
    }
}
