package ir.payment.gateway.service;

import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import ir.payment.gateway.external.service.SampleMicroService;
import ir.payment.grpc.generalmessage.GeneralMessageReq;
import ir.payment.grpc.generalmessage.GeneralMessageRes;
import ir.payment.grpc.generalmessage.GeneralMessageServiceGrpc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class GeneralMessageClient {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(GeneralMessageClient.class);

    private GeneralMessageServiceGrpc.GeneralMessageServiceBlockingStub messageServiceBlockingStub;

    public final SampleMicroService sampleMicroService;

    GeneralMessageClient(SampleMicroService sampleMicroService) {
        this.sampleMicroService = sampleMicroService;
    }

    @PostConstruct
    private void init() {
        ManagedChannel managedChannel = ManagedChannelBuilder
                .forAddress("localhost", 6565).usePlaintext().build();

        messageServiceBlockingStub =
                GeneralMessageServiceGrpc.newBlockingStub(managedChannel);
    }

    public String pepISOTerminalSwitch(ByteString msg, String source
            , String destination) {
        try {
            GeneralMessageReq messageReq = GeneralMessageReq.newBuilder()
                    .setMsg(msg)
                    .setSource(source)
                    .setDestination(destination)
                    .build();

            LOGGER.info("client sending {}", msg);

            //sampleMicroService.processPosSentMessage(messageReq);

            // TODO this line has errors
            GeneralMessageRes response =
                    messageServiceBlockingStub.pepIsoTerminalSwitch(messageReq);
            LOGGER.info("client received {}", response);
            return response.toString();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return e.getMessage();
        }
    }

    public String pepISOTerminalSwitchJustPayload(String payload) {
        LOGGER.info("pepISOTerminalSwitchJustPayload payload {}", payload);

        return payload;
    }
}
